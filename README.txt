# Tests Scripts

## Demon builder

test.py ajoute deux programmes à construire

    cd scripts-tests
    sudo python3 test.py

On peut ensuite lancer le demon-builder pour regarder comment il les gère
On peut suivre le status en temps réel sur la BDD
Le mot de passe par default de mariadb est 'root'

    sudo mysql -u root -p
    use plc_manager;
    select program_id,status from programs;

Sinon si on veut le faire a la main:

    cd scripts-tests
    sudo puthon3
    CreateGroup("Test")
    PushProgram(group_id,"Program1","../tests/program.st")
