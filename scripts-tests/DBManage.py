from DBConnect import Connect


"""
   Create a new group and return ID
"""
def CreateGroup(name):
    connection = Connect()
    cursor = connection.cursor()

    cursor.execute(
        "INSERT INTO groups (name) VALUES (%s)",
        (name,)
    )

    id = cursor.lastrowid

    connection.commit()
    cursor.close()
    connection.close()
    return id



"""
   Create new program and return ID
"""

def PushProgram(group_id,program_name,file_name):
    with open(file_name,'rb') as f:file_content = f.read()

    connection = Connect()
    cursor = connection.cursor()

    cursor.execute(
        "INSERT INTO programs (group_id,name,source,status) values (%s,%s,%s,1)",
        (group_id, program_name, file_content)
    )

    id = cursor.lastrowid

    connection.commit()
    cursor.close()
    connection.close()

    return id

