import MySQLdb

def Connect():

    conn = MySQLdb.connect(
        host="localhost",
        user="web_application",
        passwd="",
        db="plc_manager"
    )

    return conn
